package kz.kaspi.hibernate.DAO;


import kz.kaspi.entity.OrderHW;
import org.hibernate.Session;
import org.hibernate.Transaction;
import kz.kaspi.hibernate.util.HibernateUtil;

import java.util.List;


public abstract class BaseDAO implements OrderDAO{
    private Session currentSession;

    private Transaction currentTransaction;

    public Session openSession() {
        currentSession = HibernateUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeSession() {
        currentTransaction.commit();
        currentSession.close();
    }


    public Session getCurrentSession() {
        return currentSession;
    }
}
