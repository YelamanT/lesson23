package kz.kaspi.hibernate.DAO;

import kz.kaspi.entity.OrderHW;
import org.hibernate.Session;

import java.util.List;

public interface OrderDAO {

    public Session openSession();
    public void closeSession();

    public void persist(OrderHW entity);

    public void update(OrderHW entity);

    public OrderHW findById(Long id);

    public void delete(OrderHW entity);

    public List findAll();

    public void deleteAll();

    public void saveOrUpdate(OrderHW entity);
}
