package kz.kaspi.hibernate.DAO;

import kz.kaspi.entity.OrderHW;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class OrderDAOlmpl extends BaseDAO{

    @Override
    public void persist(OrderHW entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(OrderHW entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public OrderHW findById(Long id) {
        return getCurrentSession().get(OrderHW.class, id);
    }

    @Override
    public void delete(OrderHW entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<OrderHW> findAll() {

        return getCurrentSession().
                createQuery("from OrderHW").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(d -> delete(d));
    }

    @Override
    public void saveOrUpdate(OrderHW entity) {
        getCurrentSession().saveOrUpdate(entity);
    }

}
