package kz.kaspi.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.Date;

@Entity
public class OrderHW {

    public OrderHW(Long id, String customerName, Float sum, int orderId, LocalDate orderDate) {
        this.id = id;
        this.customerName = customerName;
        this.sum = sum;
        this.orderId = orderId;
        this.orderDate = orderDate;
    }

    public OrderHW() {
    }

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Long id;

    private String customerName;

    private Float sum;

    private int orderId;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate orderDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Float getSum() {
        return sum;
    }

    public void setSum(Float sum) {
        this.sum = sum;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", customerName='" + customerName + '\'' +
                ", sum=" + sum +
                ", orderId=" + orderId +
                ", orderDate=" + orderDate +
                '}';
    }
}
