package kz.kaspi.controller;


import kz.kaspi.entity.OrderHW;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/order")
public interface OrderController {

    @GetMapping
    String listAll(Model model);



    @GetMapping("/new")
    String newOrderForm( Model model);

    @PostMapping
    String saveOrUpdate(OrderHW order, Model model);

    @GetMapping("/delete/{id}")
    String delete(@PathVariable Long id, Model model);

    @GetMapping("/ascending")
    String byAscending(Model model);

    @GetMapping("/descending")
    String byDescending(Model model);

    @GetMapping("/update/{id}")
    String getOne(@PathVariable Long id, Model model);
}
