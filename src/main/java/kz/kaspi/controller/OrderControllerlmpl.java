package kz.kaspi.controller;

import kz.kaspi.entity.OrderHW;
import kz.kaspi.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;


@Controller
public class OrderControllerlmpl implements OrderController{

    @Autowired
    private OrderService service;

    private String returnList(Model model) {
        List<OrderHW> orders = service.listAll();
        model.addAttribute("orders", orders);
        return "list";
    }

    @Override
    public String listAll(Model model) {

        return returnList(model);
    }



    @Override
    public String newOrderForm( Model model) {

        model.addAttribute("mode", "New");
        model.addAttribute("modeTitle", "Add new");
        return "new";
    }

    @Override
    public String saveOrUpdate(OrderHW order, Model model) {
        if (order.getSum()<=0 || order.getOrderId()<=0 || order.getCustomerName().isBlank()
                || order.getOrderDate()==null) {
            model.addAttribute("message", "Input fields cannot be empty. Input digits " +
                    "cannot be less or equal to zero. Blank spaces without any letters are not allowed");
            return "Error";
        }

        service.saveOrUpdate(order);

        return "redirect:order";
    }

    @Override
    public String getOne(Long id, Model model) {

        OrderHW order = service.findById(id);
        if (order==null) {
            model.addAttribute("message", "Order is not found");
            return "Error";
        }
        model.addAttribute("order", order);
        model.addAttribute("mode", "Update");
        model.addAttribute("modeTitle", "Update existing");
        return "new";
    }

    @Override
    public String delete(Long id, Model model) {
        if (service.findById(id)==null) {
            model.addAttribute("message", "Order is not found");
            return "Error";
        }
        service.delete(id);

        return returnList(model);
    }

    @Override
    public String byAscending (Model model) {
        List<OrderHW> orders = service.listAll();
        Collections.sort(orders, new Comparator<OrderHW>() {
            @Override
            public int compare(OrderHW o1, OrderHW o2) {
                return Float.compare(o1.getSum(),o2.getSum());
            }
        });
        model.addAttribute("orders", orders);
        return "list";
    }

    @Override
    public String byDescending (Model model) {
        List<OrderHW> orders = service.listAll();
        Collections.sort(orders, new Comparator<OrderHW>() {
            @Override
            public int compare(OrderHW o1, OrderHW o2) {
                return Float.compare(o1.getSum(),o2.getSum());
            }
        });
        Collections.reverse(orders);
        model.addAttribute("orders", orders);
        return "list";
    }


}
