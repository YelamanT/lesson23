package kz.kaspi.service;


import kz.kaspi.entity.OrderHW;
import kz.kaspi.hibernate.DAO.OrderDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderDAO orderDAO;

    public List<OrderHW> listAll () {
        orderDAO.openSession();
        List list = orderDAO.findAll();
        orderDAO.closeSession();
        return list;
    }



    public OrderHW findById (Long id) {
        orderDAO.openSession();
        OrderHW order =orderDAO.findById(id);
        orderDAO.closeSession();
        return order;
    }



    public void delete(Long id) {
        orderDAO.openSession();
        OrderHW order = orderDAO.findById(id);
        orderDAO.delete(order);
        orderDAO.closeSession();
    }

    public void saveOrUpdate(OrderHW order) {
        orderDAO.openSession();
        orderDAO.saveOrUpdate(order);
        orderDAO.closeSession();
    }


}
