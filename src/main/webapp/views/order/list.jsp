<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<html>
<head>
    <title>All orders</title>
</head>
<body>
<div align="center">
<h1> List of all Orders</h1>



<table border ="1" cellpadding="5">
    <tr>
        <th>ID</th>
        <th>Customer's Name</th>
        <th>Total sum</th>
        <th>Order's ID</th>
        <th>Date of Order</th>
    </tr>

    <c:forEach var="orders" items="${orders}">
        <tr>
            <th>${orders.id}</th>
            <th>${orders.customerName}</th>
            <th><fmt:formatNumber type="number" value="${orders.sum}" pattern ="0.00" /> </th>
            <th>${orders.orderId}</th>
            <th>${orders.orderDate}</th>
            <th>
                <a href="<%=request.getContextPath()%>/order/delete/${orders.id}"> Delete </a>
                <a href="<%=request.getContextPath()%>/order/update/${orders.id}"> Edit </a>
            </th>


        </tr>
    </c:forEach>


</table>
    <a href="<%=request.getContextPath()%>/order/new">Add new order</a>
    <a href="<%=request.getContextPath()%>/order/descending"> Sort by descending </a>
    <a href="<%=request.getContextPath()%>/order/ascending"> Sort by ascending </a>
    <a href="<%=request.getContextPath()%>/order">Show original form</a>
</div>
</body>
</html>
