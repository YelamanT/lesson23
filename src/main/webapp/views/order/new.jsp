<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${modeTitle} Order</title>
</head>
<body>
<div align="center">
<h1> ${mode} Order</h1>

<form action="<%=request.getContextPath()%>/order" method="post">
    <input type="hidden" name="id" value="${order.id}">

    <table>
        <tr>
            <td>Client's name:</td>
            <td><input type="text" name="customerName" value="${order.customerName}" required/></td>
        </tr>
        <tr>
            <td>Sum of an order:</td>
            <td><input type="number" step = "0.01" name="sum" value="${order.sum}" required/></td>
        </tr>
        <tr>
            <td>Order's ID:</td>
            <td><input type="number" name="orderId" value="${order.orderId}" required/></td>
        </tr>
        <tr>
            <td>Order date:</td>
            <td><input type="date" name="orderDate" value="${order.orderDate}" required/></td>
        </tr>

    </table>

    <input type="submit" value="Save">

</form>
</div>
</body>
</html>
